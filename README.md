# Client application

## Install dependencies
```
npm install
```

### Compiles and hot-reloads for development
```
npm run serve
```

### Build for development
```
npm run build-dev
```

### Build and deploy for production
```
npm run build-prod
firebase deploy
```

### Customize configuration
See [Configuration Reference](https://cli.vuejs.org/config/).
