import Vue from "vue";
import App from "./App.vue";
import router from "./router";
import store from "./store";
import "./registerServiceWorker";

import firebase from 'firebase/app'
import 'firebase/firestore'

Vue.config.productionTip = false;
let firebaseConfig = require('./firebase.config.js');

firebase.initializeApp(firebaseConfig);

firebase.firestore().enablePersistence()
  .catch(function (err) {
    if (err.code === 'failed-precondition') {
      // probably multible tabs open at once
      console.log('persistance failed');
    } else if (err.code === 'unimplemented') {
      // lack of browser support for the feature
      console.log('persistance not available');
    }
  });

new Vue({
  router,
  store,
  render(h) {
    return h(App);
  },
}).$mount("#app");
