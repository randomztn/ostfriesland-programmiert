const DataTransform = require("node-json-transform").DataTransform;

const mapper = (data, mapping) => {
  let isArray = Array.isArray(data);
  if (!isArray) {
    data = [data];
  }

  let dataTransform = DataTransform(data, mapping);
  let result = dataTransform.transform();

  return (isArray ? result : result[0]);
};

module.exports = {
  mapper
};
