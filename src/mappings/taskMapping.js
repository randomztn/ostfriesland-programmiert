let taskMapProfile = {
  item: {
    id: "id",
    name: "name",
    description: "description",
    level: "level",
    helpInfo: "helpInfo",
    maxPoints: "maxPoints",
    taskString: "taskString",
    theory: "theory",
    courseId: "course.id",
    course: "course",
    userData: "userData"
  },
  operate: [
    {
      run: function (val) {
        return new Promise(resolve => {
          val.get().then(snapshot => {
            resolve(snapshot.data());
          });
        })
      }, on: "course"
    }
  ]
};

module.exports = {
  taskMapProfile
};
