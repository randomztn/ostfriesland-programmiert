const firebase = require('firebase');

let userTaskMap = {
    item: {
        id: 'id',
        endedDate: 'endedDate',
        gotPoints: 'gotPoints',
        taskId: 'task.id',
        tries: 'tries',
        userId: 'userId'
    }
};

module.exports = {
    userTaskMap
};
