const firebase = require('firebase');

export class TaskFilter {
  constructor(filters) {
    this.filters = {
      courseId: null,
      ...filters
    };
  }

  filterCollection(firebaseRef) {
    if (this.filters.courseId) {
      const courseRef = firebase.firestore().collection('courses').doc(this.filters.courseId);
      firebaseRef = firebaseRef.where('course', '==', courseRef);
    }

    return firebaseRef;
  }
}
