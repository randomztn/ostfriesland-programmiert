import Vue from "vue";
import Router from "vue-router";
import store from './store/index'

Vue.use(Router);

const notAuthenticated = (to, from, next) => {
  if (!store.getters.isAuthenticated) {
    next();
    return
  }
  next('/profile');
};

const isAuthenticated = (to, from, next) => {
  if (store.getters.isAuthenticated) {
    next();
    return
  }
  next('/login')
};

export default new Router({
  mode: "history",
  base: process.env.BASE_URL,
  routes: [
    {
      path: "/",
      name: "Home",
      component() {
        return import("./views/Home.vue");
      },
      beforeEnter: notAuthenticated
    },
    {
      path: "/login",
      name: "Login",
      component() {
        return import("./views/Login.vue");
      },
      beforeEnter: notAuthenticated
    },
    {
      path: "/register",
      name: "Register",
      component() {
        return import("./views/Register.vue");
      },
      beforeEnter: notAuthenticated
    },
    {
      path: "/profile",
      name: "Profile",
      component() {
        return import("./views/Profile.vue");
      },
      beforeEnter: isAuthenticated
    },
    {
      path: "/top",
      name: "TopUsers",
      component() {
        return import("./views/TopUsers.vue");
      },
      beforeEnter: isAuthenticated
    },
    {
      path: "/course/:id",
      name: "CourseMap",
      component() {
        return import("./views/CourseMap.vue");
      },
      props: true,
      beforeEnter: isAuthenticated
    },
    {
      path: "/task/:courseId/:id",
      name: "Task",
      component() {
        return import("./views/Task.vue");
      },
      props: true,
      children: [
        {
          path: 'theory',
          name: 'TaskTheory',
          component() {
            return import("./views/partial/task/Theory.vue");
          },
          props: true
        },
        {
          path: 'workflow',
          name: 'TaskWorkflow',
          component() {
            return import("./views/partial/task/Workflow.vue");
          },
          props: true
        },
        {
          path: 'result',
          name: 'TaskResult',
          component() {
            return import("./views/partial/task/Result.vue");
          },
          props: true
        },
      ],
      beforeEnter: isAuthenticated
    },
    {
      path: "/:slug",
      name: "StaticPage",
      component() {
        return import("./views/StaticPage.vue");
      },
      props: true
    },
    {
      path: '/404',
      alias: '*',
      name: "NotFound",
      component() {
        return import("./views/NotFound.vue");
      }
    }
  ],
});
