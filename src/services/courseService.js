import TaskService from "./taskService";

const firebase = require('firebase');

class CourseService {
  constructor() {
    this.taskService = TaskService.getInstance();
  }

  async GetEntities() {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('courses')
        .get()
        .then(async (snapshot) => {
          let courses = [];

          snapshot.forEach(doc => {
            let data = doc.data();
            data['id'] = doc.id;

            firebase.firestore().collection('courses').doc(doc.id)
              .collection('badges').get().then(snapshot => {
              let badges = [];

              snapshot.forEach(badge => {
                badges.push(badge.data());
              });

              data['badges'] = badges;
              courses.push(data);
            });
          });

          resolve(courses);
        })
        .catch(error => reject(error))
    })
  }

  async GetById(id) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection("courses").doc(id)
        .get()
        .then(course => {
          if (course.exists) {
            let data = course.data();
            data['id'] = course.id;

            resolve(data);
          }

          reject(new Error('Course doesnt exist!'));
        })
        .catch((err) => {
          reject(new Error(err));
        });
    })
  }

  async GetUserCourseData(userId) {
    return new Promise((resolve, reject) => {
      let coursesData = [];

      let userTaskRef = firebase.firestore().collection("userTasks")
        .where('userId', '==', userId);

      userTaskRef.get().then(userSnapshot => {
        let taskRef = firebase.firestore().collection("tasks");

        let userTasksData = [];

        userSnapshot.forEach(u => {
          let data = u.data();

          userTasksData.push({
            taskId: data.task.id, userData: {
              endedDate: data.endedDate,
              points: data.gotPoints,
              tries: data.tries,
              userId: data.userId
            }
          });
        });

        taskRef.get().then(taskSnapshot => {
          taskSnapshot.forEach(task => {
            let data = task.data();

            let courseData = coursesData.find(x => x.id === data.course.id);
            let userTaskData = userTasksData.filter(x => x.taskId === task.id);

            if (userTaskData.length <= 0) {
              userTaskData = [{taskId: task.id, userData: null}];
            }

            if (!courseData) {
              coursesData.push({id: data.course.id, tasks: userTaskData});
            } else {
              courseData.tasks = courseData.tasks.concat(userTaskData);
            }
          });

          resolve(coursesData);
        })
      })
    })
  }
}

class Singleton {
  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new CourseService();
    }

    return Singleton.instance;
  }
}

export default Singleton;
