const firebase = require('firebase');

class IdentityService {
  constructor() {
  }

  async Auth(userData, isRemembered) {
    return new Promise((resolve, reject) => {
      const persistence = isRemembered
        ? firebase.auth.Auth.Persistence.LOCAL
        : firebase.auth.Auth.Persistence.SESSION;

      firebase.auth().setPersistence(persistence).then(() => {
        firebase.auth().signInWithEmailAndPassword(userData.email, userData.password)
          .then(resp => {
            resp.user.getIdToken().then(function (token) {
              resolve({
                'uid': resp.user.uid,
                'token': token
              })
            });
          })
          .catch(function (error) {
            return reject(error);
          })
      });
    });
  }

  async Register(registerData) {
    return new Promise(((resolve, reject) => {
      firebase.auth().createUserWithEmailAndPassword(registerData.email, registerData.password)
        .then(resp => {
          let userDataRef = firebase.firestore().collection('userData').doc();

          let userData = {
            userId: resp.user.uid,
            firstName: registerData.firstName,
            lastName: registerData.lastName,
            rate: 0, //TODO: may be give some points for register?
            email: registerData.email
          };

          userDataRef.set(userData);

          resp.user.getIdToken().then(function (token) {
            resolve({
              'uid': resp.user.uid,
              'token': token
            });
          });
        })
        .catch((error) => {
          reject(new Error(error));
        })
    }))
  }

  async LogOut() {
    return new Promise((resolve, reject) => {
      firebase.auth().signOut()
        .then(() => {
          resolve();
        })
        .catch((error) => {
          reject(new Error(error));
        })
    })
  }
}

class Singleton {
  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new IdentityService();
    }

    return Singleton.instance;
  }
}

export default Singleton;
