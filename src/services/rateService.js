const firebase = require('firebase');

class RateService {
  constructor() {

  }

  async GetUserRates() {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('rates').get().then((snapshot) => {
        const rates = [];
        let userRef = firebase.firestore().collection('userData');
        snapshot.forEach(result => {
          let data = result.data();
          userRef.where('userId', '==', data.userId);
          rates.push(data);
        });
        userRef.get().then((userDataSnapshot) => {
          userDataSnapshot.forEach(result => {
            let data = result.data();
            rates.find(x => x.userId === data.userId)
              .userName = `${data.firstName}  ${data.latName}`;
          });
          resolve(rates);
        });
      }).catch(err => reject(err));
    });
  }
}

//   async GetUserRates() {
//     return new Promise((resolve, reject) => {
//       firebase.firestore().collection('rates').get().then((snapshot) => {
//         const rates = [];
//         snapshot.forEach(result => {
//           let data = result.data();
//           data['id'] = result.id;
//           rates.push(data);
//         });
//         resolve(rates);
//       }).catch(err => reject(err));
//     });
//   }
// }

class Singleton {
  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new RateService();
    }

    return Singleton.instance;
  }
}

export default Singleton;
