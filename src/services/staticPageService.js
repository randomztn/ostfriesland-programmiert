const firebase = require('firebase');

class StaticPageService {
  constructor() {

  }

  async GetStaticPage(slug) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('staticPage')
        .where('slug', '==', slug)
        .limit(1)
        .get()
        .then((snapshot) => {
          if(snapshot.docs.length > 0)
            resolve(snapshot.docs[0].data());
          else reject(new Error('Not found'))
      })
        .catch(err => reject(err));
    });
  }
}

class Singleton {
  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new StaticPageService();
    }

    return Singleton.instance;
  }
}

export default Singleton;
