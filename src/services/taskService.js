import { parseUserResults, getPointsByTries } from "../utils/common";
import { getValidatorByMode } from "../validators";

const firebase = require("firebase");
const {userTaskMap} = require("../mappings/userTaskMapping");

const {mapper} = require("../mappings/mapper");
const {taskMapProfile} = require("../mappings/taskMapping");

class TaskService {
  constructor() {
  }

  async GetEntities(userId, filter = null) {
    return new Promise((resolve, reject) => {
      let ref = firebase.firestore().collection("tasks");

      if (filter) ref = filter.filterCollection(ref);

      ref
        .get()
        .then(async snapshot => {
          let tasks = [];

          let userTaskRef = firebase.firestore().collection("userTasks");

          snapshot.forEach(doc => {
            let data = doc.data();
            data["id"] = doc.id;

            userTaskRef.where(
              "task",
              "==",
              firebase
                .firestore()
                .collection("tasks")
                .doc(doc.id)
            );

            tasks.push({
              userData: null,
              ...data,
            });
          });

          userTaskRef
            .where("userId", "==", userId)
            .get()
            .then(snapshot => {
              snapshot.forEach(doc => {
                let data = doc.data();

                if (tasks.find(x => x.id === data.task.id))
                  tasks.find(x => x.id === data.task.id).userData = {
                    endedDate: data.endedDate,
                    points: data.gotPoints,
                    tries: data.tries,
                  };
              });

              resolve(mapper(tasks, taskMapProfile));
            });
        })
        .catch(error => reject(error));
    });
  }

  async GetById(id) {
    return new Promise((resolve, reject) => {
      firebase
        .firestore()
        .collection("tasks")
        .doc(id)
        .get()
        .then(task => {
          if (task.exists) {
            let data = task.data();
            data["id"] = task.id;

            resolve(mapper(data, taskMapProfile));
          }

          reject(new Error("Task doesnt exist!"));
        })
        .catch(err => {
          reject(new Error(err));
        });
    });
  }

  async GetTaskPlayground(taskId) {
    return new Promise((resolve, reject) => {
      let taskRef = firebase
        .firestore()
        .collection("tasks")
        .doc(taskId);

      firebase
        .firestore()
        .collection("taskPlayground")
        .where("taskId", "==", taskRef)
        .limit(1)
        .get()
        .then(snapshot => {
          if (snapshot.size <= 0)
            reject(new Error("Task playground doesnt exist!"));

          snapshot.forEach(doc => {
            let data = doc.data();
            data["id"] = doc.id;

            Promise.all([
              doc.ref.collection('answers').get(),
              doc.ref.collection('playgrounds').get(),
              doc.ref.collection('sources').get(),
              doc.ref.collection('results').get(),
            ]).then(collections => {
              collections.forEach(snapshot => {
                if (snapshot.size > 0) {
                  let collectionName = snapshot.docs[0].ref.parent.id;
                  let collectionData = [];

                  snapshot.forEach(doc => {
                    collectionData.push(doc.data());
                  });

                  data[collectionName] = collectionData;
                }
              });

              resolve(data);
            });
          });
        })
        .catch(err => {
          reject(new Error(err));
        });
    });
  }

  async ValidateTaskResult(taskPlayground, userTask, userResults, maxPoints, currentPoints) {
    return new Promise(async (resolve, reject) => {
      const userResultsParsed = parseUserResults(userResults);
      const isEnded = userTask.endedDate !== null;
      const newTries = userTask.tries + 1;

      const { answers } = taskPlayground;

      const userErrors = [];

      answers.every((answer, i) => {
        const validate = getValidatorByMode(answer.mode);

        // для простоты предполагаем, что порядок ответов на сервере и приходящих в метод от юзера СОВПАДАЮТ
        const result = validate(userResultsParsed[i].code, answer.code);

        if(!result)
          userErrors.push({
            mode: answer.mode,
            answer: answer.code
          });

        return result;
      });

      if (userErrors.length === 0) {
        const points = isEnded ? 0 : getPointsByTries(maxPoints, newTries);

        if (isEnded) {
          resolve({...userTask, gotPoints: points});
        } else {
          firebase
            .firestore()
            .collection("userTasks")
            .doc(userTask.id)
            .update({tries: newTries, endedDate: new Date(), gotPoints: points});

          firebase
            .firestore()
            .collection("userData")
            .doc(localStorage.userDataId)
            .update({rate: points + currentPoints});

          resolve({...userTask, tries: newTries, endedDate: new Date(), gotPoints: points});
        }
      } else {
        if (!isEnded) {
          firebase
            .firestore()
            .collection("userTasks")
            .doc(userTask.id)
            .update({tries: newTries});
        }

        reject(JSON.stringify({userTask: {...userTask, tries: newTries}, userErrors: userErrors}));
      }
    });
  }

  async StartUserTask(taskId) {
    return new Promise((resolve, reject) => {
      let taskRef = firebase
        .firestore()
        .collection("tasks")
        .doc(taskId);

      firebase
        .firestore()
        .collection("userTasks")
        .where("task", "==", taskRef)
        .where("userId", "==", localStorage.currentUserId)
        .get()
        .then(snapshot => {
          if (snapshot.docs.length <= 0) {
            let userTask = {
              endedDate: null,
              gotPoints: 0,
              task: taskRef,
              tries: 0,
              userId: localStorage.currentUserId,
            };

            firebase
              .firestore()
              .collection("userTasks")
              .add(userTask)
              .then(ref => {
                let data = {...userTask};
                data["id"] = ref.id;
                resolve(mapper(data, userTaskMap));
              })
              .catch(err => reject(new Error(err)));
          } else {
            let data = snapshot.docs[0].data();
            data["id"] = snapshot.docs[0].id;
            resolve(mapper(data, userTaskMap));
          }
        });
    });
  }
}

class Singleton {
  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new TaskService();
    }

    return Singleton.instance;
  }
}

export default Singleton;
