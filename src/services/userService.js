const firebase = require("firebase");

class UserService {
  constructor() {
  }

  async GetUserData(uid) {
    return new Promise((resolve, reject) => {
      firebase
        .firestore()
        .collection("userData")
        .where("userId", "==", uid)
        .limit(1)
        .get()
        .then(snapshot => {
          let data = snapshot.docs[0].data();
          data["id"] = snapshot.docs[0].id;

          if (data.rate > 0)
            firebase.firestore().collection('userData')
              .where('rate', '>', data.rate)
              .orderBy('rate', 'desc')
              .get().then((snapshot) => {
              data['place'] = (snapshot.size + 1);
              resolve(data);
            });
          else
            resolve(data);
        })
        .catch(error => reject(error));
    });
  }

  async GetUserRateData(rateCount = 10) {
    return new Promise((resolve, reject) => {
      firebase.firestore().collection('userData')
        .orderBy('rate', 'desc').limit(rateCount)
        .where('rate', '>', 0)
        .get().then((snapshot) => {
        let userDataRates = [];
        snapshot.forEach(result => {
          let data = result.data();
          userDataRates.push({
            currentUser: localStorage.userDataId === result.id,
            userName: `${data.firstName} ${data.lastName}`,
            rate: data.rate,
          });
        });
        resolve(userDataRates);
      }).catch(err => reject(err));
    });
  }
}

class Singleton {
  static getInstance() {
    if (!Singleton.instance) {
      Singleton.instance = new UserService();
    }

    return Singleton.instance;
  }
}

export default Singleton;
