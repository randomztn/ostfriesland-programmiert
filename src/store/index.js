import Vue from 'vue';
import Vuex from 'vuex';
import auth from './modules/auth';
import course from './modules/course';
import task from './modules/task';
import userData from './modules/userData';
import rate from './modules/rate';
import staticPage from './modules/staticPage';

const debug = (process.env.NODE_ENV !== 'production');

Vue.use(Vuex);

export default new Vuex.Store({
  modules: {
    auth,
    course,
    task,
    userData,
    rate,
    staticPage
  },
  strict: debug,
});
