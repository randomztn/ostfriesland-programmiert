import {
  AUTH_ERROR,
  AUTH_LOGOUT,
  AUTH_REQUEST,
  AUTH_SUCCESS,
  GET_USER_DATA,
  REGISTER_ERROR,
  REGISTER_REQUEST,
  REGISTER_SUCCESS,
} from "../actions/auth";
import UserService from "../../services/userService";
import IdentityService from "../../services/identityService";

const state = {
  userService: UserService.getInstance(),
  identityService: IdentityService.getInstance(),

  token: localStorage.token || "",
  currentUserId: localStorage.currentUserId || null,
  currentUserData: {
    id: localStorage.userDataId || null,
    rate: 0,
    firstName: "",
    lastName: "",
  },
  authStatus: "",
};

const getters = {
  getToken: state => state.token,
  getCurrentUserId: state => state.currentUserId,
  getCurrentUserData: state => state.currentUserData,
  isAuthenticated: state => state.token || false,
  getAuthStatus: state => state.authStatus,
};

const actions = {
  [AUTH_REQUEST]: ({ commit, dispatch }, authData) => {
    return new Promise((resolve, reject) => {
      commit(AUTH_REQUEST);

      state.identityService
        .Auth(authData, authData.rememberMe)
        .then(resp => {
          commit(AUTH_SUCCESS, resp);

          resolve(resp);
        })
        .catch(error => {
          commit(AUTH_ERROR);
          reject(error);
        });
    });
  },
  [GET_USER_DATA]: ({ commit }, uid) => {
    return new Promise((resolve, reject) => {
      state.userService
        .GetUserData(uid)
        .then(resp => {
          commit(GET_USER_DATA, resp);
          resolve(resp);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  [REGISTER_REQUEST]: ({ commit, dispatch }, registerData) => {
    return new Promise((resolve, reject) => {
      commit(REGISTER_REQUEST);

      state.identityService
        .Register(registerData)
        .then(resp => {
          commit(REGISTER_SUCCESS, resp);
          resolve(resp);
        })
        .catch(error => {
          commit(REGISTER_ERROR);
          reject(error);
        });
    });
  },
  [AUTH_LOGOUT]: ({ commit }) => {
    return new Promise((resolve, reject) => {
      state.identityService
        .LogOut()
        .then(resp => {
          commit(AUTH_LOGOUT, resp);
          resolve(resp);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
};

const mutations = {
  [AUTH_REQUEST]: state => {
    state.authStatus = "auth_loading";
  },
  [AUTH_SUCCESS]: (state, data) => {
    state.authStatus = "auth_success";
    state.token = data.token;

    localStorage.token = data.token;
    localStorage.currentUserId = data.uid;
  },
  [AUTH_ERROR]: state => {
    state.authStatus = "auth_error";
  },
  [AUTH_LOGOUT]: state => {
    state.token = "";
    localStorage.token = "";
  },
  [REGISTER_REQUEST]: state => {
    state.authStatus = "register_loading";
  },
  [REGISTER_SUCCESS]: (state, data) => {
    state.authStatus = "register_success";
    state.token = data.token;

    localStorage.token = data.token;
    localStorage.currentUserId = data.uid;
  },
  [REGISTER_ERROR]: state => {
    state.authStatus = "register_error";
  },
  [GET_USER_DATA]: (state, data) => {
    state.currentUserData = data;
    localStorage.userDataId = data.id;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
