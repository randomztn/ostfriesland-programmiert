import {
  COURSE_REQUEST,
  COURSE_SUCCESS,
  GET_COURSE,
  GET_USER_COURSE_DATA,
  USER_COURSE_DATA_SUCCESS
} from "../actions/course";
import CourseService from "../../services/courseService";

const state = {
  courseService: CourseService.getInstance(),
  courses: [],
  userCourseData: []
};

const getters = {
  getCourses: state => state.courses,
  getUserCourseData: state => state.userCourseData
};

const actions = {
  [COURSE_REQUEST]: ({commit, dispatch}) => {
    return new Promise((resolve, reject) => {
      state.courseService.GetEntities().then((courseResp) => {
        dispatch(GET_USER_COURSE_DATA, localStorage.currentUserId)
          .then(courseDataResp => {
            courseResp.forEach(course => {
              let data = courseDataResp.find(x => x.id === course.id);

              if (data) {
                let taskData = data.tasks.map(task => (task.userData != null && task.userData.endedDate != null) ? 1 : 0);
                let completedTasks = taskData.reduce((a, b) => a + b, 0);

                course.percents = completedTasks > 0 ? (completedTasks / data.tasks.length * 100) : 0;
              }
            });

            commit(COURSE_SUCCESS, courseResp);

            resolve(courseResp);
          });
      })
        .catch(error => {
          reject(error);
        });
    });
  },
  [GET_COURSE]: ({commit}, courseId) => {
    return new Promise((resolve, reject) => {
      state.courseService.GetById(courseId).then((courseResp) => {
        resolve(courseResp);
      })
        .catch(error => {
          reject(error);
        });
    });
  },
  [GET_USER_COURSE_DATA]: ({commit}, userId) => {
    return new Promise((resolve, reject) => {
      state.courseService.GetUserCourseData(userId).then((courseDataResp) => {
        commit(USER_COURSE_DATA_SUCCESS, courseDataResp);

        resolve(courseDataResp);
      })
        .catch(error => {
          reject(error);
        });
    });
  }
};

const mutations = {
  [COURSE_SUCCESS]: (state, data) => {
    state.courses = data;
  },
  [USER_COURSE_DATA_SUCCESS]: (state, data) => {
    state.userCourseData = data;
  }
};

export default {
  state,
  getters,
  actions,
  mutations
};
