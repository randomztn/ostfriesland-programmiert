import RateService from "../../services/rateService";
import {RATE_DATA_REQUEST, RATE_DATA_SUCCESS} from "../actions/rate";

const state = {
  rates: [],
  rateService: RateService.getInstance()
};
const getters = {
  getRates: state => state.rates
};
const actions = {
  [RATE_DATA_REQUEST]:(commit)=>{
    return new Promise((resolve, reject) =>{
      state.rateService.GetUserRates().then(rateResp=>{
        commit(RATE_DATA_SUCCESS, rateResp);
        resolve(rateResp);
      })
    })
  }
};
const mutations = {
  [RATE_DATA_SUCCESS]:(state,rates) => {
    state.rates = rates;
  }
};

export default {state, getters, actions, mutations};
