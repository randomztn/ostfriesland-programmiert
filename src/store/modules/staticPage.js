import {STATIC_PAGE_REQUEST, STATIC_PAGE_SUCCESS} from "../actions/staticPage";
import StaticPageService from "../../services/staticPageService";

const state = {
  staticPageService: StaticPageService.getInstance(),
  currentStaticPage: {
    title: '',
    text: '',
    slug: ''
  }
};

const getters = {
  getCurrentStaticPage: state => state.currentStaticPage
};

const actions = {
  [STATIC_PAGE_REQUEST]: ({commit}, slug) => {
    return new Promise((resolve, reject) => {
      state.staticPageService.GetStaticPage(slug)
        .then((resp) => {
          commit(STATIC_PAGE_SUCCESS, resp);
          resolve(resp);
        })
        .catch(err => reject(err));
    });
  }
};

const mutations = {
  [STATIC_PAGE_SUCCESS]: (state, resp) => {
    state.currentStaticPage = resp;
  }
};

export default {
    state,
    getters,
    actions,
    mutations
};
