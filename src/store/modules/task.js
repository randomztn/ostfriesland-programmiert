import {
  GET_TASK,
  GET_TASK_HEADER_DATA,
  TASK_PLAYGROUND_REQUEST,
  USER_TASK_START,
  TASK_REQUEST,
  TASK_SUCCESS,
  VALIDATE_USER_ANSWER,
  UPDATE_USER_TASK,
} from "../actions/task";
import TaskService from "../../services/taskService";
import { GET_COURSE } from "../actions/course";

const state = {
  taskService: TaskService.getInstance(),
  tasks: [],
  currentUserTask: null,
};

const getters = {
  getTasks: state => state.tasks,
  getCurrentUserTask: state => state.currentUserTask,
};

const actions = {
  [TASK_REQUEST]: ({ commit }, filter) => {
    return new Promise((resolve, reject) => {
      state.taskService
        .GetEntities(localStorage.currentUserId, filter)
        .then(tasksResp => {
          commit(TASK_SUCCESS, tasksResp);

          resolve(tasksResp);
        })
        .catch(error => {
          reject(new Error(error));
        });
    });
  },
  [GET_TASK]: ({ commit }, taskId) => {
    return new Promise((resolve, reject) => {
      state.taskService
        .GetById(taskId)
        .then(taskResp => {
          resolve(taskResp);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  [TASK_PLAYGROUND_REQUEST]: ({ commit }, taskId) => {
    return new Promise((resolve, reject) => {
      state.taskService
        .GetTaskPlayground(taskId)
        .then(taskResp => {
          resolve(taskResp);
        })
        .catch(error => {
          reject(error);
        });
    });
  },
  [USER_TASK_START]: ({ commit }, taskId) => {
    return new Promise((resolve, reject) => {
      state.taskService
        .StartUserTask(taskId)
        .then(userTask => {
          commit(USER_TASK_START, userTask);
          resolve(userTask);
        })
        .catch(err => reject(new Error(err)));
    });
  },
  [UPDATE_USER_TASK]: ({ commit }, userTask) => {
    commit(USER_TASK_START, userTask);
  },
  [VALIDATE_USER_ANSWER]: ({ commit }, data) => {
    return new Promise((resolve, reject) => {
      state.taskService
        .ValidateTaskResult(data.taskPlayground, data.userTask, data.userResults, data.maxPoints, data.currentPoints)
        .then(updatedUserTask => {
          resolve(updatedUserTask);
        })
        .catch(err => {
          reject(new Error(err));
        });
    });
  },
};

const mutations = {
  [TASK_SUCCESS]: (state, data) => {
    state.tasks = data.sort(function(t1, t2) {
      return t1.level - t2.level;
    });
  },
  [USER_TASK_START]: (state, userTask) => {
    state.currentUserTask = userTask;
  },
  [UPDATE_USER_TASK]: (state, userTask) => {
    state.currentUserTask = userTask;
  },
};

export default {
  state,
  getters,
  actions,
  mutations,
};
