import {USER_RATES_REQUEST, USER_RATES_REQUEST_SUCCESS} from "../actions/userData";
import UserService from "../../services/userService";

const state = {
  usersData: [],
  service: UserService.getInstance()
};

const getters = {
  getUsersData: state => state.usersData,
};

const actions = {
  [USER_RATES_REQUEST]: ({commit}) => {
    return new Promise((resolve, reject) => {
        state.service.GetUserRateData().then(resp => {
        commit(USER_RATES_REQUEST_SUCCESS, resp);
        resolve(resp);
      });
    })
  }
};

const mutations = {
  [USER_RATES_REQUEST_SUCCESS]: (state, resp) => {
    state.usersData = resp;
  }
};

export default {
    state,
    getters,
    actions,
    mutations
};
