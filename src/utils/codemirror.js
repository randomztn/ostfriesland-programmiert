import CodeMirror from "codemirror/lib/codemirror";

import "codemirror/mode/css/css";
import "codemirror-formatting";
import "codemirror/mode/htmlmixed/htmlmixed";
import "codemirror/lib/codemirror.css";
import "codemirror/theme/material.css";

export default class CodeMirrorHelper {
  static initCodeMirrorArea(editorElement, mode, code, readonly = false) {
    if(mode === 'html') mode = 'htmlmixed';

    let codeMirror = CodeMirror(editorElement, {
      value: code,
      mode: mode,
      tabSize: 5,
      theme: "default",
      lineNumbers: true,
      firstLineNumber: 1,
      styleActiveLine: true
    });

    if(readonly) codeMirror.setOption('readOnly', 'nocursor');

    editorElement.id = `code${Math.random()}`;
    codeMirror.autoFormatRange({line: 0, ch: 0}, {line: codeMirror.lineCount()});
    codeMirror.setSize("100%", Math.min((codeMirror.lineCount() + 1) * 25, 150));

    //code badge
    let badge = '';

    if (mode === 'htmlmixed') {
      badge = 'html';
    } else {
      badge = mode;
    }

    let badgeElement = document.createElement('span');
    badgeElement.classList.add('CodeMirror-mode-badge');
    badgeElement.textContent = badge;

    editorElement.children[0].append(badgeElement);

    return codeMirror;
  }
}
