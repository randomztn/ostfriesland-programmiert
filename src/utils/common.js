export const parseUserResults = userResults =>
  userResults.map(res => ({ ...res, code: res.code.replace(/[\r\n\s]+/gm, "") }));

export const getPointsByTries = (maxPoints, tries) => {
  switch (tries) {
    case 1:
      return maxPoints;
    case 2:
      return maxPoints * 0.5;
    default:
      return maxPoints * 0.2;
  }
};
