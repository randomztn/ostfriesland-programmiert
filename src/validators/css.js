export const validateCss = (userResult, validAnswer) =>
  validAnswer.every(sel => validateSelector(sel, userResult));

const validateSelector = (selector, userResult) => {
  const { selectorName, properties } = selector;

  const selectorIndex = userResult.lastIndexOf(selectorName);

  if (selectorIndex === -1) return false;

  const propertiesStartIndex = selectorIndex + selectorName.length + 1; // +1 because of '{'
  const propertiesEndIndex = userResult.indexOf("}", propertiesStartIndex);
  const userProperties = userResult.slice(propertiesStartIndex, propertiesEndIndex);

  return properties.every(prop => userProperties.includes(prop));
};
