import { validateHtml } from "./html";
import { validateCss } from "./css";

export const getValidatorByMode = mode => {
  switch (mode) {
    case "html":
      return validateHtml;
    case "css":
      return validateCss;
    default:
      return () => false;
  }
};
